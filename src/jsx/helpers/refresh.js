function refreshUser() {
  if (localStorage.getItem("user")) {
    var data = JSON.parse(localStorage.getItem("user"));
    console.log(data._id);
    fetch("http://tuncoin.herokuapp.com/loggedUser/" + data._id)
      .then((res) => res.json())
      .then(
        (result) => {
          console.log(result);
          localStorage.setItem("user", JSON.stringify(result));
        },
        (error) => {
          console.log(error);
        }
      );
  }
}

export default refreshUser;
