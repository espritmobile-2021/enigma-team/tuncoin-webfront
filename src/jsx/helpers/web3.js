var Web3 = require("web3");
let metaCoinArtifact = require("./TuniCoin.json");
var Tx = require("ethereumjs-tx").Transaction;
let contract = "0x64974f26dd5c58c1c58476b0ab0439bbf3fe786d";
let BlockChain = {
  web3: null,
  account: null,
  meta: null,

  start: async function () {
    this.web3 = new Web3(
      new Web3.providers.HttpProvider(
        "https://ropsten.infura.io/v3/d595e4c3fc6d41218e7193c1ecc2f6a8"
      )
    );
    try {
      // get contract instance
      const networkId = await this.web3.eth.net.getId();
      const deployedNetwork = metaCoinArtifact.networks[networkId];
      this.meta = await new this.web3.eth.Contract(
        metaCoinArtifact.abi,
        deployedNetwork.address
      );
      if (localStorage.getItem("account")) {
      }
      const acc = this.web3.eth.accounts.privateKeyToAccount(
        "871e743d97cdfdadf973dc783cfa4726a7d77a00d34964f44dfd9f134417519d"
      );
      this.account = acc;
    } catch (error) {
      console.log(error);
      //console.error("Could not connect to contract or chain.");
    }
  },

  refreshBalance: async function (address) {
    if (address) {
      const { balanceOf } = await this.meta.methods;
      const balance = await balanceOf(address).call();
      return balance;
    }
  },

  createAccount: async function () {
    let account = this.web3.eth.accounts.create();
    return account;
  },

  sendCoin: async function (receiver, sender, amount, value, eth) {
    console.log("Sender : " + sender, "\nReceiver : " + receiver);
    if (value == "topup") {
      sender = this.account.address;
    }
    const { transfer } = this.meta.methods;
    const tx = transfer(receiver, amount);
    const rawTx = {
      nonce: await this.web3.eth.getTransactionCount(sender, "pending"),
      from: sender,
      to: eth ? receiver : contract,
      gasPrice: this.web3.utils.toHex(this.web3.utils.toWei("50", "gwei")),
      gasLimit: this.web3.utils.toHex(2100000),
      chainId: this.web3.utils.toHex(3),
    };
    console.log(tx);
    eth ? console.log("Sending Ether") : (rawTx.data = tx.encodeABI());
    eth
      ? (rawTx.value = this.web3.utils.toHex(
          this.web3.utils.toWei(amount, "ether")
        ))
      : (rawTx.value = "0x0");
    const finalTx = new Tx(rawTx, {
      chain: "ropsten",
    });
    finalTx.sign(Buffer.from(this.account.privateKey.substring(2), "hex"));
    return await this.web3.eth
      .sendSignedTransaction("0x" + finalTx.serialize().toString("hex"))
      .once("transactionHash", function (hash) {
        console.log("txHash", hash);
      })
      .once("receipt", function (receipt) {
        console.log("receipt", receipt);
      })
      .on("error", function (error) {
        console.log("error", error);
      })
      .then(function (receipt) {
        console.log("trasaction mined!", receipt);
      });
    // const tx = await transfer(receiver, amount)
    //   .send({
    //     from: sender.address,
    //   })
    //   .then(console.log);
    // return true;
  },

  getGasPrice: async function () {
    var res;
    await this.web3.eth.getGasPrice().then((value) => {
      res = this.web3.utils.fromWei(value.toString(), "ether");
    });
    return res;
  },

  getEthBalance: async function (address) {
    var balance = this.web3.eth.getBalance(address);
    return balance;
  },

  getEther: async function (value) {
    return this.web3.utils.fromWei(value, "ether");
  },

  getTransaction: async function (transaction) {
    return this.web3.eth.getTransaction(transaction);
  },

  getEncrypted: async function (password) {
    var data = JSON.parse(localStorage.getItem("publicAddress"));
    var privateKey = data.privateKey;
    return this.web3.eth.accounts.encrypt(privateKey, password);
  },

  getDecrypted: async function (keyStore, password) {
    return this.web3.eth.accounts.decrypt(keyStore, password);
  },

  getHistory: async function (address) {
    console.log(address);
    var res = await fetch(
      "http://api-ropsten.etherscan.io/api?module=account&action=tokentx&address=" +
        address +
        "&startblock=0&endblock=99999999&sort=asc&apikey=4EDVCVX5Q9UJEQPNM32MD19BQS1DB9XMAN"
    );
    var body = await res.json();
    return body;
  },
  getTokenInfo: async function () {
    var res = await fetch(
      "https://api-ropsten.etherscan.io/api?module=stats&action=tokensupply&contractaddress=0x64974f26dd5c58c1c58476b0ab0439bbf3fe786d&apikey=4EDVCVX5Q9UJEQPNM32MD19BQS1DB9XMAN"
    );
    var body = await res.json();
    return body;
  },
  loginToAccount: async function (key) {
    try {
      const acc = this.web3.eth.accounts.privateKeyToAccount(key);
      this.account = acc;
      return acc;
    } catch {
      return false;
    }
  },
};

export default BlockChain;
