import React, { useState } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import BlockChain from "../helpers/web3";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
const Login = ({ history }) => {
  const [loginData, setLoginData] = useState({});
  const handleBlur = (e) => {
    const newLoginData = { ...loginData };
    newLoginData[e.target.name] = e.target.value;
    setLoginData(newLoginData);
  };
  const submitHandler = (e) => {
    e.preventDefault();
    BlockChain.loginToAccount(e.target.privateKey.value).then((value) => {
      console.log(value);
      if (!value) {
        //ERROR
      } else {
        localStorage.setItem("publicAddress", JSON.stringify(value));
        history.push("/my-wallet");
      }
    });
  };

  const fileHandler = (e) => {
    e.preventDefault();
    var file = e.target.keyStore.files[0];
    var reader = new FileReader();
    reader.onload = function (event) {
      var keyStore = JSON.parse(reader.result);
      BlockChain.getDecrypted(keyStore, e.target.password.value).then(
        (value) => {
          console.log(value);
          if (!value) {
            //ERROR
          } else {
            localStorage.setItem("publicAddress", JSON.stringify(value));
            history.push("/my-wallet");
          }
        }
      );
    };
    reader.readAsText(file);
  };

  return (
    <div className="authincation h-100 p-meddle">
      <div className="container h-100">
        <div className="row justify-content-center h-100 align-items-center">
          <div className="col-md-6">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <h4 className="text-center mb-4">Sign in your account</h4>
                    <Tabs defaultActiveKey="key" id="uncontrolled-tab-example">
                      <Tab eventKey="key" title="Private Key">
                        <br />
                        <form action="" onSubmit={(e) => submitHandler(e)}>
                          <div className="form-group">
                            <label className="mb-1">
                              <strong>Import your wallet</strong>
                            </label>
                            <br />
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Private Key"
                              name="privateKey"
                              required
                              onChange={handleBlur}
                            />
                          </div>
                          <div className="text-center">
                            <button
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Import
                            </button>
                          </div>
                        </form>
                      </Tab>
                      <Tab eventKey="file" title="KeyStore">
                        <br />
                        <form action="" onSubmit={(e) => fileHandler(e)}>
                          <div className="form-group">
                            <label className="mb-1">
                              <strong>Import your wallet</strong>
                            </label>
                            <br />
                            <span>KeyStore file : </span>
                            <input
                              type="file"
                              className="form-control"
                              name="keyStore"
                              onChange={handleBlur}
                              required
                            />
                            <br />
                            <span>Encryption password : </span>
                            <input
                              type="password"
                              className="form-control"
                              name="password"
                              onChange={handleBlur}
                              required
                            />
                          </div>
                          <div className="text-center">
                            <button
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Import
                            </button>
                          </div>
                        </form>
                      </Tab>
                    </Tabs>
                    <div className="new-account mt-3">
                      <p>
                        Don't have a wallet?{" "}
                        <Link className="text-primary" to="/page-register">
                          Create your own.
                        </Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Login);
