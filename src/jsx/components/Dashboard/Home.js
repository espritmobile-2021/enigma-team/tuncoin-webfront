import React, { Fragment, useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import "./Home.css";
const Home = () => {
  const [news, setnews] = useState([]);
  useEffect(() => {
    fetch("http://tuncoin.herokuapp.com/cryptocurrency/news")
      .then((res) => res.json())
      .then(
        (result) => {
          setnews(result["articles"]);
          console.log(news);
        },
        (error) => {
          console.log(error);
        }
      );
  }, []);

  function getDay(value) {
    var date = new Date(value);
    return date.getDate();
  }

  function getMonth(value) {
    var date = new Date(value);
    return date.toLocaleString("default", { month: "long" });
  }
  if (!localStorage.getItem("publicAddress"))
    return <Redirect to="/page-login" />;
  else
    return (
      <Fragment>
        <div className="container">
          <main>
            <div className="row">
              {news.map((e, i) => {
                return (
                  <div className="normal">
                    <div className="module">
                      <a href={e.url}>
                        <div className="thumbnail">
                          <img src={e.urlToImage} />
                          <div className="date">
                            <div>{getDay(e.publishedAt)}</div>
                            <div>{getMonth(e.publishedAt)}</div>
                          </div>
                        </div>
                        <div className="content">
                          <div className="category">{e.source.name}</div>
                          <h1 className="title">{e.title}</h1>
                          <div className="description">{e.description}</div>
                        </div>
                      </a>
                    </div>
                  </div>
                );
              })}
            </div>
          </main>
        </div>
      </Fragment>
    );
};

export default Home;
