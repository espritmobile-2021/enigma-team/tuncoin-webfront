import React, { Fragment, useState, useRef, useEffect } from "react";
import { Dropdown } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";

const MarketCapital = () => {
  const [data, setData] = useState(
    document.querySelectorAll("#marketCapital tbody tr")
  );
  const sort = 5;
  const activePag = useRef(0);
  const [test, settest] = useState(0);
  const [list, setlist] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);

  // Active data
  const chageData = (frist, sec) => {
    for (var i = 0; i < data.length; ++i) {
      if (i >= frist && i < sec) {
        data[i].classList.remove("d-none");
      } else {
        data[i].classList.add("d-none");
      }
    }
  };

  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });

  // use effect
  useEffect(
    () => {
      setData(document.querySelectorAll("#marketCapital tbody tr"));
      fetch("http://tuncoin.herokuapp.com/cryptocurrency/values")
        .then((res) => res.json())
        .then(
          (result) => {
            setlist(result["values"]);
            setIsLoaded(true);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            console.log(error);
            setIsLoaded(false);
          }
        );
    },
    [test],
    [],
    [isLoaded]
  );

  // Active pagginarion
  activePag.current === 0 && chageData(0, sort);
  // paggination
  let paggination = Array(Math.ceil(data.length / sort))
    .fill()
    .map((_, i) => i + 1);

  // Active paggination & chage data
  const onClick = (i) => {
    activePag.current = i;
    chageData(activePag.current * sort, (activePag.current + 1) * sort);
    settest(i);
  };
  if (!localStorage.getItem("publicAddress"))
    return <Redirect to="/page-login" />;
  else
    return (
      <Fragment>
        <div className="form-head d-flex mb-3 mb-md-5 align-items-center ">
          <div className="input-group search-area d-inline-flex">
            <div className="input-group-append">
              <span className="input-group-text">
                <i className="flaticon-381-search-2" />
              </span>
            </div>
            <input
              type="text"
              className="form-control"
              placeholder="Search here"
            />
          </div>
          <Dropdown className="dropdown ml-auto d-block">
            <Dropdown.Toggle
              variant=""
              className="btn bg-white d-flex align-items-center rounded-0 svg-btn i-false"
            >
              <span className="text-primary">USD</span>
              <i className="fa fa-angle-down scale5 ml-3 mr-0" />
            </Dropdown.Toggle>
            <Dropdown.Menu
              className="dropdown-menu dropdown-menu-right"
              x-placement="bottom-end"
              style={{
                position: "absolute",
                willChange: "transform",
                top: 0,
                left: 0,
                transform: "translate3d(108px, 56px, 0px)",
              }}
            >
              <Link className="dropdown-item" to="#">
                4 June 2020 - 4 July 2020
              </Link>
              <Link className="dropdown-item" to="#">
                5 july 2020 - 4 Aug 2020
              </Link>
            </Dropdown.Menu>
          </Dropdown>

          <Dropdown className="dropdown ml-3 d-block">
            <Dropdown.Toggle
              variant=""
              className="btn bg-white i-false d-flex align-items-center rounded-0 svg-btn "
              data-toggle="dropdown"
            >
              <svg
                className="scale5"
                width={16}
                height={16}
                viewBox="0 0 28 28"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M22.4281 2.856H21.8681V1.428C21.8681 0.56 21.2801 0 20.4401 0C19.6001 0 19.0121 0.56 19.0121 1.428V2.856H9.71606V1.428C9.71606 0.56 9.15606 0 8.28806 0C7.42006 0 6.86006 0.56 6.86006 1.428V2.856H5.57206C2.85606 2.856 0.560059 5.152 0.560059 7.868V23.016C0.560059 25.732 2.85606 28.028 5.57206 28.028H22.4281C25.1441 28.028 27.4401 25.732 27.4401 23.016V7.868C27.4401 5.152 25.1441 2.856 22.4281 2.856V2.856ZM5.57206 5.712H22.4281C23.5761 5.712 24.5841 6.72 24.5841 7.868V9.856H3.41606V7.868C3.41606 6.72 4.42406 5.712 5.57206 5.712V5.712ZM22.4281 25.144H5.57206C4.42406 25.144 3.41606 24.136 3.41606 22.988V12.712H24.5561V22.988C24.5841 24.136 23.5761 25.144 22.4281 25.144Z"
                  fill="#A5A5A5"
                />
              </svg>
              <div className="text-left ml-3">
                <span className="d-block fs-14 text-black">Filter Periode</span>
              </div>
              <i className="fa fa-angle-down scale5 ml-4" />
            </Dropdown.Toggle>
            <Dropdown.Menu
              align="right"
              className="dropdown-menu dropdown-menu-right"
            >
              <Link className="dropdown-item" to="/market-capital">
                4 June 2020 - 4 July 2020
              </Link>
              <Link className="dropdown-item" to="/market-capital">
                5 july 2020 - 4 Aug 2020
              </Link>
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <div className="row">
          <div className="col-xl-12">
            <div className="table-responsive table-hover fs-14">
              <div id="marketCapital" className="dataTables_wrapper no-footer">
                <table
                  className="table display mb-4 dataTablesCard dataTable no-footer"
                  id="example5"
                  role="grid"
                  aria-describedby="example5_info"
                >
                  <thead>
                    <tr role="row">
                      <th
                        className="sorting_asc"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-sort="ascending"
                        aria-label="Rank: activate to sort column descending"
                        style={{ width: 66 }}
                      >
                        Rank
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Coin: activate to sort column ascending"
                        style={{ width: 151 }}
                      >
                        Coin
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Last Price: activate to sort column ascending"
                        style={{ width: 93 }}
                      >
                        Last Price
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Change (24h): activate to sort column ascending"
                        style={{ width: 116 }}
                      >
                        Change (24h)
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Volume (24h): activate to sort column ascending"
                        style={{ width: 147 }}
                      >
                        Volume (24h)
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Graph: activate to sort column ascending"
                        style={{ width: 308 }}
                      >
                        Graph
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label=": activate to sort column ascending"
                        style={{ width: 49 }}
                      />
                    </tr>
                  </thead>
                  <tbody>
                    {list.map((e, i) => {
                      return (
                        <tr role="row" className="odd">
                          <td className="width sorting_1">
                            <span className="bgl-primary text-primary d-inline-block p-3 fs-20 font-w600">
                              #{i + 1}
                            </span>
                          </td>
                          <td>
                            <div className="font-w600 d-flex align-items-center">
                              <img
                                className="mr-2"
                                width={40}
                                height={40}
                                src={e.logo_url}
                              ></img>
                              {e.name}
                            </div>
                          </td>
                          <td>
                            <span className="font-w600">
                              {formatter.format(e.price)}
                            </span>
                          </td>
                          <td>
                            {e["1d"].price_change_pct > 0 ? (
                              <span className="font-w500 text-success">
                                {e["1d"].price_change_pct}%
                              </span>
                            ) : (
                              <span className="font-w500 text-danger">
                                {e["1d"].price_change_pct}%
                              </span>
                            )}
                          </td>
                          <td>
                            <span className="font-w500">
                              {formatter.format(e["1d"].volume)}
                            </span>
                          </td>
                          <td></td>
                          <td>
                            <Dropdown className="dropdown float-right custom-dropdown mb-0">
                              <Dropdown.Toggle
                                variant=""
                                className="btn sharp pr-0 tp-btn i-false"
                                data-toggle="dropdown"
                                aria-expanded="false"
                              >
                                <svg
                                  width={24}
                                  height={24}
                                  viewBox="0 0 24 24"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                >
                                  <path
                                    d="M12 13C12.5523 13 13 12.5523 13 12C13 11.4477 12.5523 11 12 11C11.4477 11 11 11.4477 11 12C11 12.5523 11.4477 13 12 13Z"
                                    stroke="#2E2E2E"
                                    strokeWidth={2}
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                  />
                                  <path
                                    d="M12 6C12.5523 6 13 5.55228 13 5C13 4.44772 12.5523 4 12 4C11.4477 4 11 4.44772 11 5C11 5.55228 11.4477 6 12 6Z"
                                    stroke="#2E2E2E"
                                    strokeWidth={2}
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                  />
                                  <path
                                    d="M12 20C12.5523 20 13 19.5523 13 19C13 18.4477 12.5523 18 12 18C11.4477 18 11 18.4477 11 19C11 19.5523 11.4477 20 12 20Z"
                                    stroke="#2E2E2E"
                                    strokeWidth={2}
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                  />
                                </svg>
                              </Dropdown.Toggle>
                              <Dropdown.Menu
                                align="right"
                                className="dropdown-menu dropdown-menu-right"
                              >
                                <Link
                                  className="dropdown-item"
                                  to="/market-capital"
                                >
                                  Details
                                </Link>
                                <Link
                                  className="dropdown-item text-danger"
                                  to="/market-capital"
                                >
                                  Cancel
                                </Link>
                              </Dropdown.Menu>
                            </Dropdown>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>

                <div className="d-sm-flex text-center justify-content-between">
                  <div
                    className="dataTables_info text-black"
                    id="example5_info "
                  >
                    Showing {activePag.current * sort + 1} to{" "}
                    {data.length > (activePag.current + 1) * sort
                      ? (activePag.current + 1) * sort
                      : data.length}{" "}
                    of {data.length} entries{" "}
                  </div>
                  <div
                    className="dataTables_paginate paging_simple_numbers"
                    id="example5_paginate"
                  >
                    <Link
                      to="/market-capital"
                      className="paginate_button previous disabled"
                      onClick={() =>
                        activePag.current > 0 && onClick(activePag.current - 1)
                      }
                    >
                      Previous
                    </Link>
                    <span>
                      {paggination.map((number, i) => (
                        <Link
                          to="/market-capital"
                          key={i}
                          className={`paginate_button  ${
                            activePag.current === i ? "current" : ""
                          }`}
                          onClick={() => onClick(i)}
                          aria-controls="example5"
                        >
                          {number}
                        </Link>
                      ))}
                    </span>
                    <Link
                      to="/market-capital"
                      className="paginate_button next"
                      onClick={() =>
                        activePag.current + 1 < paggination.length &&
                        onClick(activePag.current + 1)
                      }
                    >
                      Next
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
};

export default MarketCapital;
