import React, { useEffect, useState, useRef } from "react";
import { Nav, Tab } from "react-bootstrap";
import { Link } from "react-router-dom";
import bit_1 from "../../../images/svg/bitcoin-1.svg";
import tunicoin from "../../../images/tunicoin.png";
import { Card, Col, Button, Modal } from "react-bootstrap";
import BlockChain from "../../helpers/web3";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { css } from "@emotion/react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";

var QRCode = require("qrcode.react");
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
const Wallet = () => {
  const [user, setUser] = useState("");
  let [color, setColor] = useState("#ffffff");
  const [basicModal, setBasicModal] = useState(false);
  const [modalCentered, setModalCentered] = useState(false);
  const [infoModal, setInfoModal] = useState(false);
  let [loading, setLoading] = useState(true);
  const [balance, setBalance] = useState();
  const [copied, setCopied] = useState("");
  const privateKey = useRef("");
  const [eth, setEth] = useState(false);
  const stripe = useStripe();
  const elements = useElements();
  const [tokenInfo, setInfo] = useState("");
  const [gasPrice, setGasPrice] = useState("");
  useEffect(() => {
    if (localStorage.getItem("publicAddress")) {
      const data = JSON.parse(localStorage.getItem("publicAddress"));
      setUser(data);
    } else setUser("");
  }, []);

  useEffect(() => {
    BlockChain.start().then(() => {
      BlockChain.refreshBalance(user.address).then((value) => {
        setBalance(value);
      });
      BlockChain.getTokenInfo().then((value) => {
        setInfo(value.result);
      });
      BlockChain.getGasPrice().then((value) => {
        setGasPrice(value);
      });
    });
  }, [user]);

  async function topUp(e) {
    e.preventDefault();
    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);

    // Use your card Element with other Stripe.js APIs
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
    });

    if (error) {
      console.log("[error]", error);
    } else {
      console.log("[PaymentMethod]", paymentMethod);
      console.log(user.address);
      BlockChain.sendCoin(
        user.address,
        "",
        e.target.amount.value,
        "topup",
        false
      );
      setBasicModal(false);
    }
  }
  function send(e) {
    e.preventDefault();
    setLoading(!loading);
    BlockChain.sendCoin(
      e.target.id.value,
      user.address,
      e.target.amount.value,
      "",
      eth
    );
  }

  // if (localStorage.getItem("publicAddress") == undefined)
  //   return <Redirect to="/page-login" />;
  // else
  return (
    <div className="sweet-loading">
      <div className="row">
        <div className="col-xl-6">
          <div className="row">
            <div className="col-xl-12">
              <div className="card">
                <Tab.Container defaultActiveKey="crypto-Wallet">
                  <div className="card-header d-block d-sm-flex border-0 pb-0">
                    <div>
                      <h4 className="fs-20 text-black">My current infos</h4>
                      <p className="mb-0 fs-13">
                        Here you can find your coin balance
                      </p>
                    </div>
                    <div className="card-action card-tabs mt-3 mt-sm-0">
                      <Nav className="nav nav-tabs tabs-lg">
                        <Nav.Item>
                          <Nav.Link
                            className=" px-3 wspace-no"
                            eventKey="crypto-Wallet"
                            onClick={() => {
                              !eth
                                ? BlockChain.getEthBalance(user.address).then(
                                    (res) => {
                                      BlockChain.getEther(res).then((value) =>
                                        setBalance(value)
                                      );
                                      setEth(true);
                                    }
                                  )
                                : BlockChain.refreshBalance(user.address).then(
                                    (value) => {
                                      setBalance(value);
                                      setEth(false);
                                    }
                                  );
                            }}
                          >
                            {eth ? "Switch to TuniCoin" : "Switch to Eth"}
                          </Nav.Link>
                        </Nav.Item>
                      </Nav>
                    </div>
                  </div>
                </Tab.Container>

                {/* <!-- Modal --> */}
                <Modal className="fade" show={basicModal}>
                  <Modal.Header>
                    <Modal.Title>Top Up</Modal.Title>
                    <Button
                      variant=""
                      className="close"
                      onClick={() => setBasicModal(false)}
                    >
                      <span>&times;</span>
                    </Button>
                  </Modal.Header>
                  <Modal.Body>
                    <form id="topup" onSubmit={(e) => topUp(e)}>
                      <span>Card info : </span>
                      <CardElement
                        options={{
                          style: {
                            base: {
                              fontSize: "16px",
                              color: "#424770",
                              "::placeholder": {
                                color: "#aab7c4",
                              },
                            },
                            invalid: {
                              color: "#9e2146",
                            },
                          },
                        }}
                      />
                      <br />
                      <span>
                        Enter the amount of {eth ? "Ether" : "coins"} you would
                        like to buy :{" "}
                      </span>
                      <input
                        type="number"
                        className="form-control input-default "
                        id="amount"
                        name="amount"
                        placeholder="amount"
                      />
                    </form>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button
                      onClick={() => setBasicModal(false)}
                      variant="danger light"
                    >
                      Close
                    </Button>
                    <Button
                      type="submit"
                      form="topup"
                      variant="primary"
                      disabled={!stripe}
                    >
                      Confirm
                    </Button>
                  </Modal.Footer>
                </Modal>
                <Modal className="fade" show={modalCentered}>
                  <Modal.Header>
                    <Modal.Title>Wallet QR Code</Modal.Title>
                    <Button
                      onClick={() => setModalCentered(false)}
                      variant=""
                      className="close"
                    >
                      <span>&times;</span>
                    </Button>
                  </Modal.Header>
                  <Modal.Body>
                    <center>
                      <QRCode value={user.address} />
                    </center>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button
                      onClick={() => setModalCentered(false)}
                      variant="danger light"
                    >
                      Close
                    </Button>
                    <Button variant="primary">Save changes</Button>
                  </Modal.Footer>
                </Modal>
                <Modal className="fade" show={infoModal}>
                  <Modal.Header>
                    <Modal.Title>Top Up</Modal.Title>
                    <Button
                      variant=""
                      className="close"
                      onClick={() => setInfoModal(false)}
                    >
                      <span>&times;</span>
                    </Button>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="input-group mb-3">
                      <input
                        type="text"
                        className="form-control input-default "
                        disabled
                        ref={privateKey}
                        value={user.privateKey}
                      />
                      <div className="input-group-append">
                        <CopyToClipboard
                          text={user.privateKey}
                          onCopy={() => setCopied(true)}
                        >
                          <span
                            className="iconify"
                            data-icon="bi-clipboard"
                            data-inline="false"
                            width="24"
                            height="28"
                          ></span>
                        </CopyToClipboard>
                      </div>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button
                      onClick={() => setInfoModal(false)}
                      variant="danger light"
                    >
                      Close
                    </Button>
                    <Button type="submit" form="topup" variant="primary">
                      Confirm
                    </Button>
                  </Modal.Footer>
                </Modal>
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-5 mb-4">
                      <p className="mb-2">Coin</p>
                      <h4 className="text-black">
                        {eth ? "Ethereum" : "TuniCoin"}
                      </h4>
                    </div>
                    <div className="col-sm-7 mb-4">
                      <p className="mb-2">Balance</p>
                      <h4 className="text-black">{balance}</h4>
                    </div>
                    <div className="col-sm-5 mb-4">
                      <p className="mb-2">Wallet ID</p>
                      <h4 className="text-black">{user.address}</h4>
                    </div>
                    <div className="col-sm-7 mb-4">
                      <p className="mb-2">Private Key</p>
                      <h4 className="text-black">
                        <Link to="#" onClick={() => setInfoModal(true)}>
                          <button className="btn btn-danger">
                            Reveal private Key <br />
                            (at own risk)
                          </button>
                        </Link>
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-6">
          <div className="row">
            <div className="col-xl-12">
              <div className="card">
                <div className="card-body p-0">
                  <div className="media p-4 top-up-bx col-md-12 align-items-center">
                    <div className="media-body">
                      <h3 className="fs-20 text-white">TOP UP</h3>
                      <p className="text-white font-w200 mb-0 fs-14">
                        You can buy more TuniCoins from here.
                      </p>
                    </div>
                    <Link
                      to="#"
                      onClick={() => setBasicModal(true)}
                      className="icon-btn ml-3"
                    >
                      <svg
                        width={15}
                        height={27}
                        viewBox="0 0 15 27"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M5.9375 6.23198L5.9375 24.875C5.9375 25.6689 6.58107 26.3125 7.375 26.3125C8.16892 26.3125 8.8125 25.6689 8.8125 24.875L8.8125 6.23201L11.2311 8.66231L11.2311 8.66232C11.7911 9.22502 12.7013 9.22719 13.264 8.66716C13.8269 8.107 13.8288 7.1968 13.2689 6.6342L12.9145 6.98689L13.2689 6.63419L8.3939 1.73557L8.38872 1.73036L8.38704 1.72877C7.82626 1.17279 6.92186 1.17467 6.36301 1.72875L6.36136 1.73031L6.35609 1.73561L1.48109 6.63424L1.48108 6.63425C0.921124 7.19694 0.9232 8.10708 1.48597 8.66719C2.04868 9.22724 2.95884 9.22508 3.51889 8.66237L3.51891 8.66235L5.9375 6.23198Z"
                          fill="#6418C3"
                          stroke="#6418C3"
                        />
                      </svg>
                    </Link>
                  </div>
                  <br></br>
                  <div className="media p-4 top-up-bx col-md-12 align-items-center">
                    <div className="media-body">
                      <h3 className="fs-20 text-white">Expose your QR code</h3>
                      <p className="text-white font-w200 mb-0 fs-14">
                        Exposing your QR code allows other user to send you
                        TuniCoins using it.
                      </p>
                    </div>
                    <Link
                      to="#"
                      onClick={() => setModalCentered(true)}
                      className="icon-btn ml-3"
                    >
                      <svg
                        width={27}
                        height={27}
                        viewBox="0 0 27 27"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M23.6186 15.7207L23.6186 15.7207L23.6353 22.6289C23.6354 22.6328 23.6354 22.6363 23.6353 22.6396M23.6186 15.7207L23.1353 22.6341L23.6353 22.635C23.6353 22.6481 23.6347 22.6583 23.6345 22.6627L23.6344 22.6642C23.6346 22.6609 23.6351 22.652 23.6353 22.6407C23.6353 22.6403 23.6353 22.64 23.6353 22.6396M23.6186 15.7207C23.6167 14.9268 22.9717 14.2847 22.1777 14.2866C21.3838 14.2885 20.7417 14.9336 20.7436 15.7275L20.7436 15.7275L20.7519 19.1563M23.6186 15.7207L20.7519 19.1563M23.6353 22.6396C23.6329 23.4282 22.9931 24.0705 22.2017 24.0726C22.2 24.0726 22.1983 24.0727 22.1965 24.0727L22.1944 24.0727L22.1773 24.0726L15.2834 24.056L15.2846 23.556L15.2834 24.056C14.4897 24.054 13.8474 23.4091 13.8494 22.615C13.8513 21.8211 14.4964 21.179 15.2903 21.1809L15.2903 21.1809L18.719 21.1892L5.53639 8.0066C4.975 7.44521 4.975 6.53505 5.53639 5.97367C6.09778 5.41228 7.00793 5.41228 7.56932 5.97367L20.7519 19.1563M23.6353 22.6396C23.6353 22.6376 23.6353 22.6356 23.6353 22.6336L20.7519 19.1563M22.1964 24.0726C22.1957 24.0726 22.1951 24.0726 22.1944 24.0726L22.1964 24.0726Z"
                          fill="#6418C3"
                          stroke="#6418C3"
                        />
                      </svg>
                    </Link>
                  </div>
                  <div className="media p-4 top-up-bx col-md-12 align-items-center">
                    <div className="media-body">
                      <h3 className="fs-20 text-white">
                        Check your transactions
                      </h3>
                      <p className="text-white font-w200 mb-0 fs-14">
                        You can overview your transactions from here.
                      </p>
                    </div>
                    <Link to="/transactions" className="icon-btn ml-3">
                      <svg
                        width={27}
                        height={27}
                        viewBox="0 0 27 27"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M23.6186 15.7207L23.6186 15.7207L23.6353 22.6289C23.6354 22.6328 23.6354 22.6363 23.6353 22.6396M23.6186 15.7207L23.1353 22.6341L23.6353 22.635C23.6353 22.6481 23.6347 22.6583 23.6345 22.6627L23.6344 22.6642C23.6346 22.6609 23.6351 22.652 23.6353 22.6407C23.6353 22.6403 23.6353 22.64 23.6353 22.6396M23.6186 15.7207C23.6167 14.9268 22.9717 14.2847 22.1777 14.2866C21.3838 14.2885 20.7417 14.9336 20.7436 15.7275L20.7436 15.7275L20.7519 19.1563M23.6186 15.7207L20.7519 19.1563M23.6353 22.6396C23.6329 23.4282 22.9931 24.0705 22.2017 24.0726C22.2 24.0726 22.1983 24.0727 22.1965 24.0727L22.1944 24.0727L22.1773 24.0726L15.2834 24.056L15.2846 23.556L15.2834 24.056C14.4897 24.054 13.8474 23.4091 13.8494 22.615C13.8513 21.8211 14.4964 21.179 15.2903 21.1809L15.2903 21.1809L18.719 21.1892L5.53639 8.0066C4.975 7.44521 4.975 6.53505 5.53639 5.97367C6.09778 5.41228 7.00793 5.41228 7.56932 5.97367L20.7519 19.1563M23.6353 22.6396C23.6353 22.6376 23.6353 22.6356 23.6353 22.6336L20.7519 19.1563M22.1964 24.0726C22.1957 24.0726 22.1951 24.0726 22.1944 24.0726L22.1964 24.0726Z"
                          fill="#6418C3"
                          stroke="#6418C3"
                        />
                      </svg>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-6">
          <div className="row">
            <div className="col-xl-12">
              <div className="card">
                <Col xl="12" lg="12">
                  <Card className="text-white bg-primary">
                    <Card.Header>
                      <Card.Title className="text-white">
                        {eth ? "Send Ether" : "Send TuniCoins"}
                      </Card.Title>
                    </Card.Header>
                    <Card.Body>
                      <form id="send" onSubmit={(e) => send(e)}>
                        <span>Enter the reciever's public address :</span>
                        <input
                          required
                          type="text"
                          className="form-control input-default "
                          id="id"
                          name="id"
                          placeholder="Public address"
                        />
                        <span>
                          Enter the amount of {eth ? "Ether" : "coins"} you
                          would like to send :
                        </span>
                        {eth ? (
                          <input
                            required
                            type="number"
                            className="form-control input-default "
                            id="amount"
                            name="amount"
                            step="any"
                            placeholder="amount"
                          />
                        ) : (
                          <input
                            required
                            type="number"
                            className="form-control input-default "
                            id="amount"
                            name="amount"
                            min="1"
                            placeholder="amount"
                          />
                        )}
                      </form>
                    </Card.Body>
                    <Card.Footer className=" d-sm-flex justify-content-between align-items-center">
                      <Card.Text className=" text-white d-inline">
                        Pressing confirm is a one time operation.
                      </Card.Text>
                      <Button
                        className="btn btn-primary"
                        type="submit"
                        form="send"
                      >
                        Confirm
                      </Button>
                    </Card.Footer>
                  </Card>
                </Col>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-6 col-xxl-6 col-lg-6 col-sm-6">
          <div className="card overflow-hidden">
            <div className="card-header border-0 pb-0">
              <div className="mr-auto">
                <h2 className="text-black mb-2 font-w200">
                  Total supply : {tokenInfo}{" "}
                </h2>
                <span>Gas price : </span>
                <input
                  className="form-control"
                  disabled
                  value={gasPrice + " ETH"}
                />
                <br />
                <span>Number of holders : </span>
                <input className="form-control" disabled value="5" />
                <br />
                <span>Holdings :</span>
                <input
                  className="form-control"
                  disabled
                  value={(balance / tokenInfo) * 100 + "%"}
                />
              </div>
              <img src={tunicoin} alt="" height="80" width="80" />
            </div>
            <div className="card-body p-0">
              {/* <canvas id="widgetChart1" height={75} /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Wallet;
