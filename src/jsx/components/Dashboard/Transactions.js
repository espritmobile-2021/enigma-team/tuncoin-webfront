import React, { Fragment, useState, useRef, useEffect } from "react";
import { Dropdown, Modal, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import BlockChain from "../../helpers/web3";
import { CSVLink, CSVDownload } from "react-csv";
import ReactDOM from "react-dom";

const Transactions = () => {
  const [data, setData] = useState("");
  const sort = 5;
  const activePag = useRef(0);
  const [test, settest] = useState(0);
  const [transactions, setTrans] = useState([]);
  const [user, setUser] = useState("");
  const date = new Date();
  const [basicModal, setBasicModal] = useState(false);
  const [details, setDetails] = useState({});

  useEffect(() => {
    var usr = localStorage.getItem("publicAddress");
    if (user == "") {
      const data = JSON.parse(usr);
      setUser(data);
    }
    BlockChain.start().then(() => {
      var result = BlockChain.getHistory(user.address);
      result.then((value) => {
        console.log(value["result"]);
        if (value["status"] == 1) setTrans(value["result"]);
      });
    });
  }, [user]);

  // Active data
  const chageData = (frist, sec) => {
    for (var i = 0; i < data.length; ++i) {
      if (i >= frist && i < sec) {
        data[i].classList.remove("d-none");
      } else {
        data[i].classList.add("d-none");
      }
    }
  };

  // Active pagginarion
  activePag.current === 0 && chageData(0, sort);
  // paggination
  let paggination = Array(Math.ceil(transactions.length / sort))
    .fill()
    .map((_, i) => i + 1);

  // Active paggination & chage data
  const onClick = (i) => {
    activePag.current = i;
    chageData(activePag.current * sort, (activePag.current + 1) * sort);
    settest(i);
  };

  function handleDate(value) {
    var temp = new Date(value * 1000);
    return temp.toLocaleString("en-us");
  }
  if (!localStorage.getItem("publicAddress"))
    return <Redirect to="/page-login" />;
  else
    return (
      <Fragment>
        <div id="download"></div>
        <div className="form-head d-flex mb-3 mb-md-5 align-items-center ">
          <Link
            className="ml-auto rounded-0 btn bgl-primary text-primary"
            to="#"
            onClick={() => {
              console.log();
              const element = (
                <CSVDownload data={transactions} target="_blank" />
              );
              ReactDOM.render(element, document.querySelector("#download"));
            }}
          >
            <svg
              className="mr-3 scale5"
              width={16}
              height={16}
              viewBox="0 0 18 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M1.99989 23.6666H15.9999C16.6439 23.6666 17.1666 23.144 17.1666 22.5C17.1666 21.856 16.6439 21.3333 15.9999 21.3333H1.99989C1.35589 21.3333 0.833225 21.856 0.833225 22.5C0.833225 23.144 1.35589 23.6666 1.99989 23.6666ZM5.49989 9.66665V3.24998C5.49989 2.47648 5.80674 1.73447 6.3539 1.18731C6.90107 0.64014 7.64306 0.333313 8.41656 0.333313H9.58323C10.3567 0.333313 11.0987 0.64014 11.6459 1.18731C12.1919 1.73447 12.4999 2.47648 12.4999 3.24998V9.66665H15.9999C16.4712 9.66665 16.8971 9.95013 17.0779 10.3865C17.2576 10.8228 17.1584 11.3245 16.8247 11.6581L9.82473 18.6581C9.36856 19.1131 8.63006 19.1131 8.17506 18.6581L1.17506 11.6581C0.841391 11.3245 0.741063 10.8228 0.921897 10.3865C1.10273 9.95013 1.52739 9.66665 1.99989 9.66665H5.49989ZM13.1836 12H11.3332C10.6892 12 10.1666 11.4773 10.1666 10.8333C10.1666 10.8333 10.1666 5.95198 10.1666 3.24998C10.1666 3.09481 10.1047 2.94664 9.99507 2.83698C9.88657 2.72731 9.73722 2.66665 9.58323 2.66665C9.20173 2.66665 8.79806 2.66665 8.41656 2.66665C8.26139 2.66665 8.11324 2.72731 8.00357 2.83698C7.89507 2.94664 7.83323 3.09481 7.83323 3.24998V10.8333C7.83323 11.4773 7.31056 12 6.66656 12H4.81623L8.99989 16.1836L13.1836 12Z"
                fill="#6418C3"
              />
            </svg>
            Save to CSV
          </Link>
        </div>
        <div className="row">
          <div className="col-xl-12">
            <div className="table-responsive table-hover fs-14">
              <div id="transaciton" className="dataTables_wrapper no-footer">
                <table
                  className="table display mb-4 dataTablesCard card-table dataTable no-footer"
                  id="example5"
                  role="grid"
                  aria-describedby="example5_info"
                >
                  <thead>
                    <tr role="row">
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Transaction ID: activate to sort column ascending"
                        style={{ width: 147 }}
                      >
                        Transaction ID
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Date: activate to sort column ascending"
                        style={{ width: 92 }}
                      >
                        Date
                      </th>

                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Coin: activate to sort column ascending"
                        style={{ width: 104 }}
                      >
                        Coin
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Amount: activate to sort column ascending"
                        style={{ width: 63 }}
                      >
                        Amount
                      </th>
                      <th
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example5"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Status: activate to sort column ascending"
                        style={{ width: 93 }}
                      >
                        Status
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {transactions
                      .slice(0)
                      .reverse()
                      .map((e, i) => {
                        if (
                          e.contractAddress ==
                          "0x64974f26dd5c58c1c58476b0ab0439bbf3fe786d"
                        )
                          return (
                            <tr role="row" className="odd">
                              <td>#{e.hash}</td>
                              <td className="wspace-no">
                                {handleDate(e.timeStamp)}
                              </td>

                              <td className="wspace-no">TuniCoin</td>
                              <td>
                                {e.to == user.address.toLowerCase() ? (
                                  <span className="text-success font-w700">
                                    {e.value} TNC
                                  </span>
                                ) : (
                                  <span className="text-danger font-w700">
                                    {e.value} TNC
                                  </span>
                                )}
                              </td>
                              <td>
                                {e.confirmations > 0 ? (
                                  <Link
                                    onClick={() => {
                                      setBasicModal(true);
                                      setDetails(e);
                                    }}
                                    to="#"
                                    className="btn-link text-success float-right"
                                  >
                                    Confirmed
                                  </Link>
                                ) : (
                                  <Link
                                    onClick={() => {
                                      setBasicModal(true);
                                      setDetails(e);
                                    }}
                                    to="#"
                                    className="btn-link text-warning float-right"
                                  >
                                    Pending
                                  </Link>
                                )}
                              </td>
                            </tr>
                          );
                      })}
                  </tbody>
                </table>
                <div className="d-sm-flex text-center justify-content-between">
                  <div
                    className="dataTables_info text-black"
                    id="example5_info "
                  >
                    Showing {activePag.current * sort + 1} to{" "}
                    {transactions.length > (activePag.current + 1) * sort
                      ? (activePag.current + 1) * sort
                      : transactions.length}{" "}
                    of {transactions.length} entries{" "}
                  </div>
                  <div
                    className="dataTables_paginate paging_simple_numbers"
                    id="example5_paginate"
                  >
                    <Link
                      to="/transactions"
                      className="paginate_button previous disabled"
                      onClick={() =>
                        activePag.current > 0 && onClick(activePag.current - 1)
                      }
                    >
                      Previous
                    </Link>
                    <span>
                      {paggination.map((number, i) => (
                        <Link
                          to="/transactions"
                          key={i}
                          className={`paginate_button  ${
                            activePag.current === i ? "current" : ""
                          }`}
                          onClick={() => onClick(i)}
                          aria-controls="example5"
                        >
                          {number}
                        </Link>
                      ))}
                    </span>
                    <Link
                      to="/transactions"
                      className="paginate_button next"
                      onClick={() =>
                        activePag.current + 1 < paggination.length &&
                        onClick(activePag.current + 1)
                      }
                    >
                      Next
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Modal show={basicModal}>
          <Modal.Header>
            <Modal.Title>Details</Modal.Title>
            <Button
              variant=""
              className="close"
              onClick={() => setBasicModal(false)}
            >
              <span>&times;</span>
            </Button>
          </Modal.Header>
          <Modal.Body>
            <span>Receipient : </span>
            <input className="form-control" readOnly value={details.to} />
            <span>Sender : </span>
            <input className="form-control" readOnly value={details.from} />
            <span>Gas used : </span>
            <input className="form-control" readOnly value={details.gasUsed} />
            <span>TuniCoins exchanged : </span>
            <input className="form-control" readOnly value={details.value} />
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={() => setBasicModal(false)}
              variant="primary light"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
};

export default Transactions;
