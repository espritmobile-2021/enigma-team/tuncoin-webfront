import React, { setState, useState } from "react";

import { Link } from "react-router-dom";
/// Scroll

import BlockChain from "../../helpers/web3";
import { Dropdown, Modal, DropdownButton, Button } from "react-bootstrap";
import { saveAs } from "file-saver";

const Header = ({ onNote, toggle, onProfile, onNotification, onClick }) => {
  const [basicModal, setBasicModal] = useState(false);
  var path = window.location.pathname.split("/");
  var name = path[path.length - 1].split("-");
  var filterName = name.length >= 3 ? name.filter((n, i) => i > 0) : name;
  var finalName = filterName.includes("app")
    ? filterName.filter((f) => f !== "app")
    : filterName.includes("ui")
    ? filterName.filter((f) => f !== "ui")
    : filterName.includes("uc")
    ? filterName.filter((f) => f !== "uc")
    : filterName.includes("basic")
    ? filterName.filter((f) => f !== "basic")
    : filterName.includes("jquery")
    ? filterName.filter((f) => f !== "jquery")
    : filterName.includes("table")
    ? filterName.filter((f) => f !== "table")
    : filterName.includes("page")
    ? filterName.filter((f) => f !== "page")
    : filterName.includes("email")
    ? filterName.filter((f) => f !== "email")
    : filterName.includes("ecom")
    ? filterName.filter((f) => f !== "ecom")
    : filterName.includes("chart")
    ? filterName.filter((f) => f !== "chart")
    : filterName.includes("editor")
    ? filterName.filter((f) => f !== "editor")
    : filterName;

  function generate(e) {
    e.preventDefault();
    BlockChain.getEncrypted(e.target.password.value).then((data) => {
      var fileName = "keyStore.json";

      // Create a blob of the data
      var fileToSave = new Blob([JSON.stringify(data, undefined, 2)], {
        type: "application/json",
        name: fileName,
      });

      // Save the file
      saveAs(fileToSave, fileName);
    });
  }

  return (
    <div className="header">
      <div className="header-content">
        <nav className="navbar navbar-expand">
          <div className="collapse navbar-collapse justify-content-between">
            <div className="header-left">
              <div
                className="dashboard_bar"
                style={{ textTransform: "capitalize" }}
              >
                {finalName.join(" ").length === 0
                  ? "Crypto News"
                  : finalName.join(" ")}
              </div>
            </div>

            <ul className="navbar-nav header-right">
              <Dropdown className="nav-item dropdown header-profile ml-sm-4 ml-2">
                <Dropdown.Toggle
                  variant=""
                  className="nav-link i-false c-pointer"
                >
                  <div className="header-info"></div>
                  Menu
                </Dropdown.Toggle>
                <Dropdown.Menu align="right" className="mt-2">
                  <Link
                    to="#"
                    onClick={() => setBasicModal(true)}
                    className="dropdown-item ai-icon"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-save"
                      viewBox="0 0 16 16"
                    >
                      <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z" />
                    </svg>
                    <span className="ml-2">Export wallet </span>
                  </Link>
                  <Link
                    to="/page-login"
                    onClick={() => localStorage.clear()}
                    className="dropdown-item ai-icon"
                  >
                    <svg
                      id="icon-logout"
                      xmlns="http://www.w3.org/2000/svg"
                      className="text-danger"
                      width={18}
                      height={18}
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth={2}
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4" />
                      <polyline points="16 17 21 12 16 7" />
                      <line x1={21} y1={12} x2={9} y2={12} />
                    </svg>
                    <span className="ml-2">Logout </span>
                  </Link>
                </Dropdown.Menu>
              </Dropdown>
            </ul>
          </div>
        </nav>
      </div>
      <Modal className="fade" show={basicModal}>
        <Modal.Header>
          <Modal.Title>Export your account</Modal.Title>
          <Button
            variant=""
            className="close"
            onClick={() => setBasicModal(false)}
          >
            <span>&times;</span>
          </Button>
        </Modal.Header>
        <Modal.Body>
          <span>
            This will encrypt your account with a password of your choosing and
            generate a json file for use with any other wallet.
            <span style={{ color: "red" }}>
              <br />A Strong password is recommended !
            </span>
          </span>
          <form id="generate" onSubmit={(e) => generate(e)}>
            <input className="form-control" type="password" name="password" />
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setBasicModal(false)} variant="danger light">
            Close
          </Button>
          <Button type="submit" form="generate" variant="primary">
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Header;
