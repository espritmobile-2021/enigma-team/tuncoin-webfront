import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import reportWebVitals from "./reportWebVitals";
import SimpleReactLightbox from "simple-react-lightbox";
import BlockChain from "./jsx/helpers/web3";
ReactDOM.render(
  <App>
    <SimpleReactLightbox>
      <App />
    </SimpleReactLightbox>
  </App>,
  document.getElementById("root")
);
BlockChain.start();
reportWebVitals();
