# Tuncoin

A new nodejs project.

## Project Links
- **Link Backend without Blockchain:** _https://gitlab.com/mohamed.testouri/Tuncoin-Backend_
- **Link Backend with Blockchain:** _https://gitlab.com/mohamed.testouri/Tuncoin_
- **Link Mobile Appliacation:** : _https://gitlab.com/espritmobile-2021/enigma-team/tuncoin-mobile_
- **Link Frontend Web:** _https://gitlab.com/espritmobile-2021/enigma-team/tuncoin-webfront_
- **APK:** _https://drive.google.com/file/d/178ieD5R9B-FbXDYPDvPQ79b_mxgzI4uA/view?usp=sharing_

## Get Started

### Tunicoin frontend application :
  - The app was made using the latest version of ReactJS and Npm.
  - The main library that was used is Ethereum's web3js.
  - The application is a basic wallet that is compliant with the Ether standards, it does everything an Ether wallet does.
  
### Technical details :
  - The app uses the web3js library to establish a connection with an Ethereum node created by the Infura provider.
  - The app only stores user details locally, preserving the decentralized concept of a BlockChain.
  - When demanded with a task , it will use the web3js library to contact the node and execute the task.
  - Transactions are created , signed with the user's private key then sent to the BlockChain for miners to confirm it.
  - Ether or ethereum transactions and TuniCoin transactions are possible.
  - The TuniCoin is an ERC20 compliant Token that lives on the Ropsten test network of the BlockChain, the address for the contract should be below.
  
### Installation and scenario :
  - To run this project you should do :
    * npm install
    * npm start
  
  - Possible scenario : 
    1 - A user can access the registration page, input his email and receive credentials there. The credentials contain the wallet's private key that is not be     shared or lost. Once they have that , they can import their wallet from the BlockChain using the key. They can : Fill up their balance using fiat money. Send or receive both TuniCoins and Ether. Check some basic yet useful info as well as their transactions.
    2 - A user can import their KeyStore V3 encrypted account and basically do everything above.
    3 - A user with an existing account can generate a KeyStore V3 encrypted JSON file and store it.
  
  *KeyStore V3 is an encryption standard.
  
  Useful links : 
      Contract link : https://ropsten.etherscan.io/address/0x64974f26dd5c58c1c58476b0ab0439bbf3fe786d
      Buy me some coffe ? 0xb87DD5b5189EFC3Cc2770a5c52c16C6f193bdbB8
   
